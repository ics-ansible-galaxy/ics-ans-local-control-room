# ics-ans-local-control-room

Ansible playbook to install the local control room workstations.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
