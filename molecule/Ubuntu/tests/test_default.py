import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("local_control_room")


def test_installed_packages(host):
    for package in ("gnuplot", "xfce4-session", "nomachine"):
        assert host.package(package).is_installed
    assert host.package("libreoffice").is_installed


def test_installed_applications(host):
    assert host.file("/opt/OpenXAL").is_symlink
    assert host.file('/opt/OpenXAL/optics/current/main.xal').exists
    assert host.file('/opt/conda/envs/epics/epics/lib/linux-x86_64/libca.so').exists
    assert not host.file("/usr/share/applications/eclipse.desktop").exists
    assert not host.file("/usr/share/applications/gitkraken.desktop").exists
